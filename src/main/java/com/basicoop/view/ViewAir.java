package com.basicoop.view;

import com.basicoop.controller.Controller;
import com.basicoop.controller.ControllerImpl;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ViewAir {

     private Controller controller = new ControllerImpl();
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public ViewAir() {

        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - print cargo lifting");
        menu.put("2", "  2 - print flight range");
        menu.put("3", "  3 - print seat capacity");
        menu.put("4", "  4 - total capacity");
        menu.put("5", "  5 - Sort flight range");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
    }

    private void pressButton1() {
        System.out.println(controller.getCargoLifting());
    }

    private void pressButton2() {
        System.out.println(controller.getFlightRange());
    }

    private void pressButton3() {
        System.out.println(controller.getSeatCapacity());
    }

    private void pressButton4() {
        controller.totalCapacity();
    }

    private void pressButton5() {
        controller.sortFlightRange();
    }
    
    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

}
