package com.basicoop.view;

@FunctionalInterface
public interface Printable {
    void print();
}
