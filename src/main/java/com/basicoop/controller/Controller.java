package com.basicoop.controller;

import java.util.List;

public interface Controller {

    void totalLifting();
    void totalCapacity();
    void sortFlightRange();
    List<Integer> getSeatCapacity();
    List<Integer> getCargoLifting();
    List<Integer>getFlightRange();
    List<String> getName();
}
