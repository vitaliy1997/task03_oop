package com.basicoop.controller;


import com.basicoop.model.Airbus;
import com.basicoop.model.UkraineAir;

import java.util.List;

public class ControllerImpl implements Controller {

    UkraineAir ukraineAir =new Airbus();
    @Override
    public void totalLifting() {
        ukraineAir.totalLifting();
    }

    @Override
    public void totalCapacity() {
        ukraineAir.totalCapacity();
    }

    @Override
    public void sortFlightRange() {
        ukraineAir.sortFlightRange();
    }

    @Override
    public List<Integer> getSeatCapacity() {
        return ukraineAir.getSeatCapacity();
    }

    @Override
    public List<Integer> getCargoLifting() {
        return ukraineAir.getCargoLifting();
    }

    @Override
    public List<Integer> getFlightRange() {
        return ukraineAir.getFlightRange();
    }

    @Override
    public List<String> getName() {
        return ukraineAir.getName();
    }
}
