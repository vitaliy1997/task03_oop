package com.basicoop.model;

import java.util.List;

/**
 * This interface i create field and method for class Boeing and Domain
 */
public interface UkraineAir {

    List<Integer> getSeatCapacity();
    List<Integer> getCargoLifting();
    List<Integer>getFlightRange();
    List<String> getName();
    void totalLifting();
    void totalCapacity();
    void sortFlightRange();
}
