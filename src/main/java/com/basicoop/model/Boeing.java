package com.basicoop.model;

import java.util.Collections;
import java.util.List;

    public class Boeing implements UkraineAir {

        Domain domain;

        public Boeing(){
            domain=new Domain();
        }

        @Override
        public List<Integer> getSeatCapacity() {

            return domain.getSeat();
        }

        @Override
        public List<Integer> getCargoLifting() {

            return domain.getLifting();
        }

        @Override
        public List<Integer> getFlightRange() {

            return domain.getRange();
        }

        @Override
        public List<String> getName() {
            String name = "Boeing";
            return Collections.singletonList(name);
        }

        @Override
        public void totalLifting() {

        }

        @Override
        public void totalCapacity() {

        }

        @Override
        public void sortFlightRange() {

        }

    }
