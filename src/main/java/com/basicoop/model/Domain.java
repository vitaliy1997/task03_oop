package com.basicoop.model;

import java.util.*;

/**
 *  Domain
 * I create data file and sort method
 *
 * totalCapacity I discovered total seat capacity
 *
 *
 */
public class Domain {

        private List<Integer> numberRange;
        private List<Integer> numberLift;
        private List<Integer> numberSeat;
        private int amount;


        public Domain(){
            numberLift=new LinkedList<>();
            numberRange = new LinkedList<>();
            numberSeat = new LinkedList<>();
            Random rand = new Random();
            amount = (int) (15 * rand.nextDouble());
            getNumberSeat();
            getFlightRange();
            getCargoLifting();
        }

        public void getFlightRange(){
            Random rand = new Random();
            for (int i = 0; i < amount; i++) {
                numberRange.add((int) (2500 * rand.nextDouble() +200));
            }
        }

        public List<Integer> getRange(){
            return numberRange;
        }

        public void getNumberSeat(){
            Random rand = new Random();
            for (int i = 0; i < amount; i++) {
                numberSeat.add((int) (120 * rand.nextDouble() +50));
            }
         }

         public List<Integer> getSeat(){
            return numberSeat;
         }

        public void getCargoLifting(){
            Random rand = new Random();
            for (int i = 0; i < amount; i++) {
                numberLift.add((int) (100 * rand.nextDouble() +-70));
            }
        }

        public List<Integer> getLifting() {
            return numberLift;
        }

        public void totalCapacity(){
            int sumCapacity=0;
            for(int seatArray : numberSeat){
                sumCapacity+=seatArray;
            }
            System.out.println(sumCapacity);
        }

        public void totalLifting(){
            int sumLifting =0;
            for (int liftingArray : numberLift){
                sumLifting+=liftingArray;
            }
            System.out.println(sumLifting);
        }

        public void sortFlightRange(){
           Collections.sort(numberRange);
            System.out.println(numberRange);
        }
    }
