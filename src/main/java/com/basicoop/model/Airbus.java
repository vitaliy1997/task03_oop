package com.basicoop.model;

import java.util.Collections;
import java.util.List;

public class Airbus implements UkraineAir{

    Domain domain;

    public Airbus(){
        domain=new Domain();
    }
    @Override
    public List<Integer> getSeatCapacity() {

        return domain.getSeat();
    }

    @Override
    public List<Integer> getCargoLifting() {

        return domain.getLifting();
    }

    @Override
    public List<Integer> getFlightRange() {

        return domain.getRange();
    }

    @Override
    public List<String> getName() {
        String name =  "Airbus";
        return Collections.singletonList(name);
    }

    @Override
    public void totalLifting() {
        domain.getCargoLifting();
    }

    @Override
    public void totalCapacity() {
        domain.totalCapacity();
    }

    @Override
    public void sortFlightRange() {
        domain.sortFlightRange();
    }


}
